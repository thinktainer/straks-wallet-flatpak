	flatpak-builder ".build" "io.straks.Wallet.json" --repo=repo --ccache --jobs=$(nproc --ignore=1)
	flatpak --user remote-add --if-not-exists straks-wallet
	flatpak --user install straks-wallet io.straks.Wallet

